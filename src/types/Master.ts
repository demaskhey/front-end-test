export type Master = {
    id: number,
    legalId: string,
    businessName: string,
}